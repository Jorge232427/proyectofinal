from cmath import cos, sin
import math
from msilib.sequence import tables
from turtle import circle
import sympy as sym
import matplotlib.pyplot as plt
import numpy as np
'''
Proyecto final Programacion Basica

'''
def presentacion():
    print("Gravitación")
    print("Bienvendio este programa nos mostrara la trayectorio que tomara")
    print("un cuerpo determinado, dependiendo de su velocidad y de la masa del objeto que lo atraiga")
    o=(input("¿desea iniciar?"))
    if o=="si":
        y=potencial_gravitatorio() #Llamar la funcion de inicio 
        
    return y
def potencial_gravitatorio():
    h=[]
    o=float(input("masa del planeta en notacion cientifica (x10^24)"))
    p=o*10**24
    t=(p*6371)/(5.972*10**24)
    h.append(t)
    print(t)
    while True:  #el ciclo se utiiza para determinar que el radio que se ingrese no sea menor a la superficie media del plante 
        r=int(input("radio donde esta ubicado el objeto en km "))
        if t<=r:
            
            h.append(r)
            break       
    cg=(6.67191*10**-11) #constante gravitatoria 
    k=(cg*(p))/(r*1000)**2
    h.append(k)
    print(h)
    
    return h

def funcion_cuerpo(k):
    o=float(input("velocidad del cuerpo"))
    print(o)
    d=o*(((2.0*(k[0]))/k[1])**0.5)
    i=(((d**2)/k[0]))/4
    y=((x**2)/((-4)*i))+k[1]
    return y

p=presentacion()
m=(p[0]*1.1)
x=np.linspace(0,m,100000)
y=np.linspace(0,0,10000)
t=funcion_cuerpo(p)
c=(((p[0])**2)-(x**2))**0.5
plt.plot(x,t,x,c)
plt.show() 
